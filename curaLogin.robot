*** Settings ***
Documentation       Template robot main suite.
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    firefox
${id}    John Doe
${password}    ThisIsNotAPassword
${facility}    Hongkong CURA Healthcare Center
${visit_date}    02/07/2022
${comment}    un commentaire

*** Test Cases ***
Login Page
    Open Browser    ${url}    ${browser}
    Maximize Browser Window
    Click Element    id:btn-make-appointment
    Sleep    1

    #check
    Title Should Be    CURA Healthcare Service

    Input Text    id:txt-username    ${id}
    Input Password    id:txt-password    ${password}
    Click Element    id:btn-login
    Sleep    2

    # Complete the appointment form
    Select From List by Label    id:combo_facility    ${facility}
    Click Element    id:chk_hospotal_readmission
    Click Element    id:radio_program_medicaid
    Input Text    id:txt_visit_date    ${visit_date}
    Input Text    id:txt_comment    ${comment}

    # Submit the form
    Click Element    id:btn-book-appointment
    Sleep     2

    #Check
    Element Should Be Visible    //p/a[.='Go to Homepage']
    
    # Return to the homepage
    Click Element    //p/a[.='Go to Homepage']

    # Wait for page load
    Wait Until Page Contains Element    id:btn-make-appointment

    # Close the browser
    Sleep    3
    Close Browser
