*** Settings ***
Documentation       Template robot main suite.
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    firefox
# ${id}    John Doe
# ${password}    ThisIsNotAPassword
${facility}    Hongkong CURA Healthcare Center
${visit_date}    02/07/2022
${comment}    un commentaire

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${url}    ${browser}
    Maximize Browser Window
    Sleep    1
Login To Book appointment
    [Arguments]    ${id}    ${password}
    Click Element    id:btn-make-appointment
    Title Should Be    CURA Healthcare Service
    Input Text    id:txt-username    ${id}
    Input Password    id:txt-password    ${password}
    Click Element    id:btn-login
    Sleep    2
    Element Should Be Visible    id:btn-make-appointment
Return HomePage
 #Check
    Element Should Be Visible    //p/a[.='Go to Homepage']
    # Return to the homepage
    Click Element    //p/a[.='Go to Homepage']
    # Wait for page load
    Wait Until Page Contains Element    id:btn-make-appointment
End Of Test And Close Browser
    # Close the browser
    Sleep    3
    Close Browser
Book Appointment
    # Complete the appointment form
    Select From List by Label    id:combo_facility    ${facility}
    Click Element    id:chk_hospotal_readmission
    Click Element    id:radio_program_medicaid
    Input Text    id:txt_visit_date    ${visit_date}
    Input Text    id:txt_comment    ${comment}
    # Submit the form
    Click Element    id:btn-book-appointment
    Sleep     2

*** Test Cases ***
Login Page Right Test
    Open Browser To Login Page
    Login To Book appointment    John Doe    ThisIsNotAPassword
    End Of Test And Close Browser

Login Page Wrong Test
    Open Browser To Login Page
    Login To Book appointment    John Doe    aze

Book Appointment Test
    Open Browser To Login Page
    Login To Book appointment    John Doe    ThisIsNotAPassword
    Book Appointment
    Return HomePage
    End Of Test And Close Browser
